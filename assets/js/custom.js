$(document).ready(function(){
   new WOW().init();
   
    $('.dropdown').on('show.bs.dropdown', function() {
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function() {
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });

    $("#diagramOurSustainability").css('margin-top',-Math.abs($('#economicImpact').height()));

    $('.smic-tabs a').on('click', function(){
      var id = $(this).attr('href');

      $('.comm-tabs').hide();
      $('.smic-tabs a').parent().removeClass('active');
      $(this).parent().addClass('active');
      $(id).show();

    });

    $('#socialImpact > a').on('click', function (event) {
      event.preventDefault();
      $('#socialImpact').toggleClass('oops');
    });

});

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    var environmentalResponsibility = $('#environmentalResponsibility');
    var sustainableGrowth = $('#sustainableGrowth');
    var diagram = $('#diagram');
    var header = $('.smic-header nav');
    $('.smic-header ').toggleClass('scroll',
     //add 'ok' class when div position match or exceeds else remove the 'ok' class.
      scroll >= $( window ).height()
    );

    if(environmentalResponsibility.length) {
      if(scroll >= (environmentalResponsibility.offset().top - 170)) {
        $('.moving-truck ').addClass('move');
      }
    }

    if(sustainableGrowth.length) {
      if(scroll >= (sustainableGrowth.offset().top - 50)) {
        $('.moving-truck-two ').addClass('move');
      }
    }

    if(diagram) {
      // $('#environmental').offset().top
      if(scroll >= ($('#diagramEnvironmental').offset().top - header.innerHeight()) && scroll <= ($('#diagramSocial').offset().top - header.innerHeight())) {
        $("#economicImpact").addClass('rotate-env');
        $("#economicImpact").removeClass('rotate-soc');
        $("#economicImpact").removeClass('rotate-gov');
      } else if(scroll >= ($('#diagramSocial').offset().top - header.innerHeight()) && scroll <= ($('#diagramGovernance').offset().top - header.innerHeight())) {
        $("#economicImpact").removeClass('rotate-env');
        $("#economicImpact").removeClass('rotate-gov');
        $("#economicImpact").addClass('rotate-soc');
      } else if(scroll >= ($('#diagramGovernance').offset().top - header.innerHeight())) {
        $("#economicImpact").removeClass('rotate-env');
        $("#economicImpact").removeClass('rotate-soc');
        $("#economicImpact").addClass('rotate-gov');
      }
      // console.log(scroll+'---'+($('#social').offset().top - header.innerHeight())+'---'+($('#governance').offset().top - header.innerHeight()));
    }

    // if(diagram) {
    //   var diagramScrollTop = diagram.innerHeight() + diagram.offset().top;
    //   var diagramHeight    = scroll + $(window).height();

    //   // console.log(height+'----'+diagram.offset().top+'---'+scroll+'----'+$( window ).height());
    //   console.log(diagramScrollTop+'-----'+diagramHeight);

    //   if(scroll >= (diagram.offset().top) && diagramHeight <= diagramScrollTop) {
    //     $("#economicImpact").addClass('scroll');
    //   } else {
    //     // $("#economicImpact").removeClass('scroll');
    //   }
    // }

    // console.log($('#diagram').offset().top);
});

$(document).on('click', 'a[href^="#"]', function (event) {
  event.preventDefault();

  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
});